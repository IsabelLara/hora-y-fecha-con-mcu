#include <NTPClient.h> //importamos la librería del cliente NTP

#include <ESP8266WiFi.h> //librería wifi

#include <DNSServer.h>

//#include <ESP8266WebServer.h >

#include <WiFiUdp.h> // librería UDP para comunicar con 
                     // NTP

#include <WiFiManager.h> // Activación del WiFiManager para autoconectar
                        // y cargar portal cautivo de conexión


//ESP8266WebServer server(80);

//iniciamos el cliente udp para su uso con el server NTP
WiFiUDP ntpUDP;


NTPClient timeClient(ntpUDP, "0.mx.pool.ntp.org",-21600,6000);

String fechahora;

int dia, hora, minutos, segundos, fdia;

int anio, mes;

String fechacompleta;

String shora,sminutos, ssegundos;

String sanio, smes, sdia;

String fecha;

char daysOfTheWeek[7][12] = {"Dom", "Lun", "Mar", "Mie", "Jue", "Vie", "Sab"};

char meses[12][12] = {"Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Sep", "Oct", "Nov", "Dic"};

void setup(){
  fechahora = "";
  shora = "";
  sminutos = "";
  ssegundos = "";
  sanio = "";
  smes = "";
  sdia = "";
  fechacompleta = "";
  fecha = "";

  Serial.begin(9600); // activamos la conexión serial
  

  WiFiManager wifiManager;

  wifiManager.resetSettings();
  wifiManager.autoConnect("WiFi con Arduino y MCU");
  

  while ( WiFi.status() != WL_CONNECTED ) {
    delay ( 500 );
    Serial.println( "Conectando._Esperando MCU;" );
  }

  timeClient.begin(); 
}

void loop() {
  timeClient.update(); //server NTP
  
  fechacompleta = timeClient.getFormattedDate();
  int splitT = fechacompleta.indexOf("T");
  fecha = fechacompleta.substring(0, splitT);

  sanio = fecha.substring(0, 4);
  smes = fecha.substring(5, 7);
  sdia = fecha.substring(8, 10);
  smes = meses[smes.toInt()-1];
  
  dia = timeClient.getDay();
  hora = timeClient.getHours();
  minutos = timeClient.getMinutes();
  segundos = timeClient.getSeconds();

  shora = hora;
  if (hora < 10) {
    shora = "0";
    shora+=hora;
  }
  
  sminutos = minutos;
  if (minutos < 10) {
    sminutos = "0";
    sminutos+=minutos;
  }

  ssegundos = segundos;
  if (segundos < 10) {
    ssegundos = "0";
    ssegundos+=segundos;
  }
    
  fechahora=daysOfTheWeek[dia];
  fechahora+=" ";
  fechahora+=sdia;
  fechahora+=" ";
  fechahora+=smes;
  fechahora+=" ";
  fechahora+=sanio;
  fechahora+="_";
  fechahora+=shora;
  fechahora+=":";
  fechahora+=sminutos;
  fechahora+=":";
  fechahora+=ssegundos;
  fechahora+=";";

  Serial.println(fechahora);
//Imprimimos por puerto serie la hora actual

  delay(1000);
}